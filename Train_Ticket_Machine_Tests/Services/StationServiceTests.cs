﻿using Moq;
using NUnit.Framework;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using Train_Ticket_Machine.Services;
using Train_Ticket_Machine.Models;

namespace Train_Ticket_Machine_Tests.Services
{
    public class StationServiceTests
    {
        private Mock<IConfiguration> _mockConfiguration;
        private Mock<ILogger<StationService>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockConfiguration = new Mock<IConfiguration>();
            _mockLogger = new Mock<ILogger<StationService>>();

            _mockConfiguration.Setup(x => x["StationApi:Url"]).Returns("https://raw.githubusercontent.com/abax-as/coding-challenge/master/station_codes.json");
        }

        [Test]
        public void TestSearchStations()
        {
            // Arrange
            var stationService = new StationService(_mockConfiguration.Object, _mockLogger.Object);
            var expectedStations = new List<Station>
            {   new Station { stationName = "DARTON", stationCode = "DTN" },
                new Station { stationName = "DARTFORD", stationCode = "DFD" }
            };
            var expectedChars = new List<char> { 'F', 'O' };

            // Act
            var result = stationService.SearchStations("DART");

            // Assert

            Assert.AreEqual(expectedChars.FirstOrDefault(x => x.Equals("O")), result.Item2.FirstOrDefault(x => x.Equals("O")));
            Assert.AreEqual(expectedChars.FirstOrDefault(x => x.Equals("F")), result.Item2.FirstOrDefault(x => x.Equals("F")));
            Assert.AreEqual(expectedStations.FirstOrDefault(x => x.Equals("DARTFORD")), result.Item1.FirstOrDefault(x => x.Equals("DARTFORD")));
            Assert.AreEqual(expectedStations.FirstOrDefault(x => x.Equals("DARTON")), result.Item1.FirstOrDefault(x => x.Equals("DARTON")));
            Assert.AreEqual(2, result.Item1.Count);
        }
    }
}