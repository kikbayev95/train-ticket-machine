Train Ticket Machine Project
The Train Ticket Machine is a project that provides an API for fetching and searching train stations.

1.Table of Contents
2.Technologies Used
3.Setup
4.Project Structure
5.Testing
6.API Endpoints
6.Contributing

Technologies Used

This project is created with:

.NET 6.0
ASP.NET Core
Polly
Newtonsoft.Json
NUnit (for testing)
Swagger
Docker
Coverlet (for code quality)
Parallel code

Setup
To run this project, make sure you have .NET 6.0 SDK installed. You can then clone the project and run it using the dotnet run command or in docker container.

Project Structure
This project is organized into several layers, including:

Models: This contains all the data models used in the project.
Services: This includes interfaces and classes that provide services related to the retrieval and search of station data.
Controllers: This is where the API endpoints are defined.
Tests: Contains unit tests for the services and controllers.
Testing
This project uses NUnit for testing. After cloning the project, you can run tests using the dotnet test command.
You can run Coverlet for getting testcoverage result command----- dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=./results/coverage/
A total of 1 test files matched the specified pattern.

Passed!  - Failed:     0, Passed:     1, Skipped:     0, Total:     1, Duration: 467 ms - Train_Ticket_Machine_Tests.dll (net6.0)

Calculating coverage result...
  Generating report '**:\Repos\train-ticket-machine\Train_Ticket_Machine_Tests\coverage.opencover.xml'

+----------------------+--------+--------+--------+
| Module               | Line   | Branch | Method |
+----------------------+--------+--------+--------+
| Train_Ticket_Machine | 62.12% | 75%    | 66.66% |
+----------------------+--------+--------+--------+

+---------+--------+--------+--------+
|         | Line   | Branch | Method |
+---------+--------+--------+--------+
| Total   | 62.12% | 75%    | 66.66% |
+---------+--------+--------+--------+
| Average | 62.12% | 75%    | 66.66% |
+---------+--------+--------+--------+

Curent 
API Endpoints
This project provides the following API endpoints:

GET /api/stations: Retrieves all stations.

Contributing
We welcome contributions to this project. Please feel free to open an issue or submit a pull request.