﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Polly;
using Train_Ticket_Machine.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Concurrent;

namespace Train_Ticket_Machine.Services
{
    public class StationService : IStationService
    {
        private List<Station> _stations;
        private readonly string _url;
        private readonly ILogger<StationService> _logger;
        public StationService(IConfiguration configuration, ILogger<StationService> logger)
        {
            _url = configuration["StationApi:Url"];
            _logger = logger;
            LoadStationsAsync();
        }
        public async Task<List<Station>> GetStationsAsync()
        { 
            var policy = Policy.Handle<Exception>().RetryAsync(10, (exception, retryCount) =>
            {
                _logger.LogInformation($"Exception {exception.Message} on attempt {retryCount}"); 
            });

            return await policy.ExecuteAsync(async () =>
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = await client.GetStringAsync(_url);
                    return JsonConvert.DeserializeObject<List<Station>>(response);
                }
            });
        }
        private async Task LoadStationsAsync()
        {
            _stations = await GetStationsAsync();
        }
        public (List<Station>, List<char>) SearchStations(string searchString)
        {
            if (_stations == null)
            {
                LoadStationsAsync().Wait();
            }
            ConcurrentBag<Station> matchedStations = new ConcurrentBag<Station>();
            ConcurrentBag<char> nextCharacters = new ConcurrentBag<char>();

            Parallel.ForEach(_stations, station =>
            {
                if (station.stationName.StartsWith(searchString, StringComparison.OrdinalIgnoreCase))
                {
                    matchedStations.Add(station);
                    if (station.stationName.Length > searchString.Length)
                    {
                        nextCharacters.Add(station.stationName[searchString.Length]);
                    }
                }
            });

            return (matchedStations.ToList(), nextCharacters.Distinct().ToList());
        }
    }
}
