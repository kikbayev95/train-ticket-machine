﻿using Train_Ticket_Machine.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Train_Ticket_Machine.Services
{
    public interface IStationService
    {
        Task<List<Station>> GetStationsAsync();
        (List<Station>, List<char>) SearchStations(string searchString);
    }
}
