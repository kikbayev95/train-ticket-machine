﻿using System.Diagnostics.CodeAnalysis;

namespace Train_Ticket_Machine.Models
{
    [ExcludeFromCodeCoverage]
    public class Station
    {
        public string stationCode { get; set; }
        public string stationName { get; set; }
    }
}
