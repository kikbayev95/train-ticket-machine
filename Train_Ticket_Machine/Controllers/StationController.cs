﻿using Microsoft.AspNetCore.Mvc;
using Train_Ticket_Machine.Services;

namespace Train_Ticket_Machine.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StationsController : ControllerBase
    {
        private readonly IStationService _stationService; 
        public StationsController(IStationService stationService)
        {
            _stationService = stationService;
        }

        [HttpGet("{searchString}")]
        public async Task<IActionResult> Get(string searchString)
        {
            var (matchedStations, nextCharacters) = _stationService.SearchStations(searchString);
            return Ok(new { matchedStations, nextCharacters });
        }
    }
}
